package com.example.sg_haze_air_index;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;
import android.media.MediaPlayer;

/**
 * Created by GSW on 9/6/2016.
 */
public class SplashScreen extends Activity {

    MediaPlayer mp;

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 1500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        mp = MediaPlayer.create(this, R.raw.splash_music_smile_cut);
        mp.start();


        TextView tvAppName = findViewById(R.id.tvAppName);
        tvAppName.setText(R.string.app_name);

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                Intent i = new Intent(SplashScreen.this, MainActivity.class);
                startActivity(i);

                // close this activity
                mp.stop();
                finish();
            }
        }, SPLASH_TIME_OUT);

    }
}
