package com.example.sg_haze_air_index.ui.main;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.sg_haze_air_index.LabelLocation;
import com.example.sg_haze_air_index.NetworkChangeReceiver;
import com.example.sg_haze_air_index.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class PM25Fragment extends Fragment {

  private PageViewModel pageViewModel;

  public PM25Fragment() {
    // Required empty public constructor
  }

  /**
   * @return A new instance of fragment PM2.5 Fragment.
   */
  public static PM25Fragment newInstance() {
    return new PM25Fragment();
  }

  private static final String TAG = "PM2.5 Fragment";
  Gson gson = new GsonBuilder().create();

  Calendar rightNow = Calendar.getInstance();
  SimpleDateFormat mdformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
  String strDate = mdformat.format(rightNow.getTime());

  private String urlJsonObj = "https://api.data.gov.sg/v1/environment/pm25?date_time="+ strDate;


  //Serious Haze Timing
  //private String urlJsonObj = "https://api.data.gov.sg/v1/environment/psi?date_time=2019-09-18T13:00:00";
  //private String urlJsonObj = "https://api.data.gov.sg/v1/environment/psi?date_time=2019-09-18T12:00:00";


  private ArrayList<LabelLocation> labelLocationDataList = new ArrayList<>();

  String regionName = "";

  @Override
  public void onCreate(Bundle savedInstanceState) {
    Log.e("url",urlJsonObj);

    super.onCreate(savedInstanceState);
    pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);
    pageViewModel.setIndex(TAG);

    //Detect Network Connectivity
    NetworkChangeReceiver c = new NetworkChangeReceiver();
    c.onReceive(this.getContext(),null);

  }


  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {

    Log.e("Create pm25","yes");

    // Inflate the layout for this fragment
    View root = inflater.inflate(R.layout.fragment_pm25, container, false);
    final TextView tv_west = root.findViewById(R.id.tv_west);
    final TextView tv_east = root.findViewById(R.id.tv_east);
    final TextView tv_central = root.findViewById(R.id.tv_central);
    final TextView tv_north = root.findViewById(R.id.tv_north);
    final TextView tv_south = root.findViewById(R.id.tv_south);
    final TextView tv_overall = root.findViewById(R.id.tv_overall);

    final ImageView imageView_overall = root.findViewById(R.id.imageView_overall);

    //prepare data
    RequestQueue queue = Volley.newRequestQueue(getContext());
    final SharedPreferences settingDetails = getContext().getSharedPreferences("psi_settings", MODE_PRIVATE);
    SharedPreferences.Editor editor = settingDetails.edit();

    Log.e("000  check internet pm25","true");

    if(isInternetAvailable(this.getContext())) //returns true if internet available
    {
      Log.e("check internet pm25","true");

      JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, urlJsonObj, null,
              new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {

                  try {
                    String jsonResponse = "";
                    JSONArray locationValue = (JSONArray) response.get("region_metadata");

                    SharedPreferences.Editor editor = settingDetails.edit();

                    if (locationValue.length()>0) {
                      JSONArray items = (JSONArray) response.get("items");
                      for (int i = 0; i < items.length(); i++) {

                        JSONObject item = items.getJSONObject(i);

                        JSONObject readings = item.getJSONObject("readings");

                        JSONObject pm25Value = readings.getJSONObject("pm25_one_hourly");

                        tv_west.setText("West   " + pm25Value.get("west").toString());
                        tv_east.setText("East   " + pm25Value.get("east").toString());
                        tv_central.setText("Central   " + pm25Value.get("central").toString());
                        tv_north.setText("North   " + pm25Value.get("north").toString());
                        tv_south.setText("South   " + pm25Value.get("south").toString());

                        int value_west = 0;
                        value_west = Integer.parseInt(pm25Value.get("west").toString());
                        int value_east = 0;
                        value_east = Integer.parseInt(pm25Value.get("east").toString());
                        int value_central = 0;
                        value_central = Integer.parseInt(pm25Value.get("central").toString());
                        int value_south = 0;
                        value_south = Integer.parseInt(pm25Value.get("south").toString());
                        int value_north = 0;
                        value_north = Integer.parseInt(pm25Value.get("north").toString());

                        tv_west.setTextColor(checkPM25Level(value_west));
                        tv_east.setTextColor(checkPM25Level(value_east));
                        tv_central.setTextColor(checkPM25Level(value_central));
                        tv_north.setTextColor(checkPM25Level(value_north));
                        tv_south.setTextColor(checkPM25Level(value_south));

                        int overall = (int)Math.round( (value_west+value_east+value_central+value_north+value_south)/5 );

                        if (overall <= 55) {
                          imageView_overall.setImageResource(R.mipmap.pm25_good);
                          tv_overall.setText("Normal");
                        } else if (overall >= 56 && overall <= 150) {
                          imageView_overall.setImageResource(R.mipmap.pm25_unhealthy);
                          tv_overall.setText("Elevated");
                        } else if (overall >= 151 && overall <= 250) {
                          imageView_overall.setImageResource(R.mipmap.pm25_very_unhealthy);
                          tv_overall.setText("High");
                        } else if (overall >= 250) {
                          imageView_overall.setImageResource(R.mipmap.pm25_hazardous);
                          tv_overall.setText("Very High");

                        }

                        if(value_west!=0) {
                          editor.putInt("pm25_west", value_west);
                        }
                        if(value_east!=0) {
                          editor.putInt("pm25_east", value_east);
                        }
                        if(value_north!=0) {
                          editor.putInt("pm25_north", value_north);
                        }
                        if(value_south!=0) {
                          editor.putInt("pm25_south", value_south);
                        }
                        if(value_central!=0) {
                          editor.putInt("pm25_central", value_central);
                        }
                        if(overall!=0) {
                          editor.putInt("pm25_overall", overall);
                        }

                        editor.commit();
                      }
                    }
                    else{

//                      editor.putInt("pm25_west", 0);
//                      editor.putInt("pm25_east", 0);
//                      editor.putInt("pm25_north", 0);
//                      editor.putInt("pm25_south", 0);
//                      editor.putInt("pm25_central", 0);
//                      editor.putInt("pm25_overall", 0);
//                      editor.commit();

                      tv_west.setText(settingDetails.getInt("pm25_west", 0));
                      tv_east.setText(settingDetails.getInt("pm25_east", 0));
                      tv_north.setText(settingDetails.getInt("pm25_north", 0));
                      tv_south.setText(settingDetails.getInt("pm25_south", 0));
                      tv_central.setText(settingDetails.getInt("pm25_central", 0));
                      tv_overall.setText(settingDetails.getInt("pm25_overall", 0));
                    }

                  } catch (JSONException e) {
                    Log.e(TAG, e.getMessage(), e);

                    Toast.makeText(getContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                  }

                }
              },
              new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError e) {
                  Log.e(TAG, e.getMessage(), e);

                  Toast.makeText(getContext(),"Error: "+ e.getMessage(), Toast.LENGTH_SHORT).show();

                }
              }) {
        /**
         * Passing some request headers*
         */
        @Override
        public Map getHeaders() {
          HashMap headers = new HashMap();
          headers.put("AccountKey", "");
          headers.put("Accept", "application/json");

          return headers;
        }
      };

      queue.add(jsonObjReq);

    }
    else
    {
      int value_west = settingDetails.getInt("pm25_west", 0);
      int value_east = settingDetails.getInt("pm25_east", 0);
      int value_central = settingDetails.getInt("pm25_north", 0);
      int value_south = settingDetails.getInt("pm25_south", 0);
      int value_north = settingDetails.getInt("pm25_central", 0);

      tv_west.setText("West   " + String.valueOf(value_west));
      tv_east.setText("East   " + String.valueOf(value_east));
      tv_north.setText("North   " + String.valueOf(value_north));
      tv_south.setText("South   " + String.valueOf(value_south));
      tv_central.setText("Central   " + String.valueOf(value_central));

      tv_west.setTextColor(checkPM25Level(value_west));
      tv_east.setTextColor(checkPM25Level(value_east));
      tv_central.setTextColor(checkPM25Level(value_central));
      tv_north.setTextColor(checkPM25Level(value_north));
      tv_south.setTextColor(checkPM25Level(value_south));

      int overall = settingDetails.getInt("psi_overall", 0);

      if(overall <= 50){
        imageView_overall.setImageResource(R.mipmap.pm25_good);
        tv_overall.setText("Good");
      }
      else if(overall >= 51 && overall <=100){
        imageView_overall.setImageResource(R.mipmap.pm25_moderate);
        tv_overall.setText("Moderate");
      }
      else if(overall >= 101 && overall <=200){
        imageView_overall.setImageResource(R.mipmap.pm25_unhealthy);
        tv_overall.setText("Unhealthy");
      }
      else if(overall >= 201 && overall <=300){
        imageView_overall.setImageResource(R.mipmap.pm25_very_unhealthy);
        tv_overall.setText("Very unhealthy");
      }
      else if(overall > 300){
        imageView_overall.setImageResource(R.mipmap.pm25_hazardous);
        tv_overall.setText("Hazardous");

      }


      Toast.makeText(this.getContext(),"No Internet Connection",Toast.LENGTH_LONG).show();
    }

//for handling changes
//    pageViewModel.getText().observe(this, new Observer<String>() {
//      @Override
//      public void onChanged(@Nullable String s) {
//        //
//
//
//        textView.setText("K: "+regionName);
//
//      }
//     });


    return root;
  }

  private final String TAG_INTERNET = "CheckNetwork";

  public boolean isInternetAvailable(Context context)
  {
    NetworkInfo info = (NetworkInfo) ((ConnectivityManager)
            context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();

    if (info == null)
    {
      Log.d(TAG_INTERNET,"no internet connection");
      return false;
    }
    else
    {
      if(info.isConnected())
      {
        Log.d(TAG_INTERNET," internet connection available...");
        return true;
      }
      else
      {
        Log.d(TAG_INTERNET," internet connection");
        return true;
      }


    }
  }

  public int checkPM25Level(int value){
    int indicatorColor = ContextCompat.getColor(getContext(),R.color.colorWhite);

    if(value <= 55){
      indicatorColor = ContextCompat.getColor(getContext(),R.color.colorWhite);
    }
    else if(value >= 56 && value <= 150){
      indicatorColor = ContextCompat.getColor(getContext(),R.color.colorYellow);
    }
    else if(value >= 151 && value <= 250){
      indicatorColor = ContextCompat.getColor(getContext(),R.color.colorOrange);
    }
    else if(value >= 250){
      indicatorColor = ContextCompat.getColor(getContext(),R.color.colorRed);
    }

    return indicatorColor;

  }

}
