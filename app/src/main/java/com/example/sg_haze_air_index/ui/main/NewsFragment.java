package com.example.sg_haze_air_index.ui.main;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.sg_haze_air_index.NetworkChangeReceiver;
import com.example.sg_haze_air_index.R;

public class NewsFragment extends Fragment {

    private static final String TAG = "AQI";

    private PageViewModel pageViewModel;

    public NewsFragment() {
        // Required empty public constructor
    }


    public static NewsFragment newInstance() {
        return new NewsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);
        pageViewModel.setIndex(TAG);

        Log.e("News","yes");

        //Detect Network Connectivity
        NetworkChangeReceiver c = new NetworkChangeReceiver();
        c.onReceive(this.getContext(),null);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_news, container, false);

        final WebView webView = root.findViewById(R.id.wv_content);
        webView.getSettings().setJavaScriptEnabled(true);

        if(this.getContext()!=null){
            if(isInternetAvailable(this.getContext())) //returns true if internet available
            {
                webView.loadUrl("https://www.haze.gov.sg/");
            }
            else
            {
                final TextView tv_errorMessage = root.findViewById(R.id.tv_errorMessage);
                tv_errorMessage.setVisibility(View.VISIBLE);

                webView.setVisibility(View.INVISIBLE);

                //Toast.makeText(this.getContext(),"No Internet Connection",Toast.LENGTH_LONG).show();
            }
        }
        else{
            webView.setVisibility(View.INVISIBLE);
        }


        return root;
    }

    private final String TAG_INTERNET = "CheckNetwork";
    public boolean isInternetAvailable(Context context)
    {
        NetworkInfo info = (NetworkInfo) ((ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();

        if (info == null)
        {
            Log.d(TAG_INTERNET,"no internet connection");
            return false;
        }
        else
        {
            if(info.isConnected())
            {
                Log.d(TAG_INTERNET," internet connection available...");
                return true;
            }
            else
            {
                Log.d(TAG_INTERNET," internet connection");
                return true;
            }


        }
    }
}
