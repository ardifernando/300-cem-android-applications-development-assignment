package com.example.sg_haze_air_index.ui.main;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.sg_haze_air_index.LabelLocation;
import com.example.sg_haze_air_index.NetworkChangeReceiver;
import com.example.sg_haze_air_index.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class PSIFragment extends Fragment {

    private PageViewModel pageViewModel;

    public PSIFragment() {
        // Required empty public constructor
    }

    /**
     * @return A new instance of fragment PSI Fragment.
     */
    public static PSIFragment newInstance() {
        return new PSIFragment();
    }

    private static final String TAG = "PSI Fragment";
    Gson gson = new GsonBuilder().create();

    Calendar rightNow = Calendar.getInstance();
    SimpleDateFormat mdformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    String strDate = mdformat.format(rightNow.getTime());

    private String urlJsonObj = "https://api.data.gov.sg/v1/environment/psi?date_time="+ strDate;


    private ArrayList<LabelLocation> labelLocationDataList = new ArrayList<>();

    String regionName = "";

    TextView name;
    TextView email;
    public static final String mypreference = "mypref";
    public static final String vWest = "";
    public static final String vEast = "";
    public static final String vNorth = "";
    public static final String vSouth = "";
    public static final String vCentral = "";
    public static final String vOverall = "";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.e("url",urlJsonObj);

        Log.e("Create psi","yes");

        super.onCreate(savedInstanceState);
        pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);
        pageViewModel.setIndex(TAG);

        //Detect Network Connectivity
        NetworkChangeReceiver c = new NetworkChangeReceiver();
        c.onReceive(this.getContext(),null);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_psi, container, false);
        final TextView tv_west = root.findViewById(R.id.tv_west);
        final TextView tv_east = root.findViewById(R.id.tv_east);
        final TextView tv_central = root.findViewById(R.id.tv_central);
        final TextView tv_north = root.findViewById(R.id.tv_north);
        final TextView tv_south = root.findViewById(R.id.tv_south);
        final TextView tv_overall = root.findViewById(R.id.tv_overall);

        final ImageView imageView_overall = root.findViewById(R.id.imageView_overall);

        //prepare data
        RequestQueue queue = Volley.newRequestQueue(getContext());
        final SharedPreferences settingDetails = getContext().getSharedPreferences("psi_settings", MODE_PRIVATE);
        SharedPreferences.Editor editor = settingDetails.edit();

        if(isInternetAvailable(this.getContext())) //returns true if internet available
        {
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, urlJsonObj, null,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {

                            try {
                                String jsonResponse = "";
                                JSONArray locationValue = (JSONArray) response.get("region_metadata");

                                SharedPreferences.Editor editor = settingDetails.edit();

                                if (locationValue.length()>0){
                                    JSONArray items = (JSONArray) response.get("items");
                                    for (int i = 0; i < items.length(); i++) {

                                        JSONObject item = items.getJSONObject(i);

                                        JSONObject  readings = item.getJSONObject("readings");

                                        JSONObject  psiValue = readings.getJSONObject("psi_twenty_four_hourly");

                                        tv_west.setText("West   " + psiValue.get("west").toString());
                                        tv_east.setText("East   " + psiValue.get("east").toString());
                                        tv_central.setText("Central   " + psiValue.get("central").toString());
                                        tv_north.setText("North   " + psiValue.get("north").toString());
                                        tv_south.setText("South   " + psiValue.get("south").toString());

                                        int value_west = Integer.parseInt(psiValue.get("west").toString());
                                        int value_east = Integer.parseInt(psiValue.get("east").toString());
                                        int value_central = Integer.parseInt(psiValue.get("central").toString());
                                        int value_south = Integer.parseInt(psiValue.get("south").toString());
                                        int value_north = Integer.parseInt(psiValue.get("north").toString());

                                        tv_west.setTextColor(checkPSILevel(value_west));
                                        tv_east.setTextColor(checkPSILevel(value_east));
                                        tv_central.setTextColor(checkPSILevel(value_central));
                                        tv_north.setTextColor(checkPSILevel(value_north));
                                        tv_south.setTextColor(checkPSILevel(value_south));

                                        int overall = Integer.parseInt(psiValue.get("national").toString());

                                        if(overall <= 50){
                                            imageView_overall.setImageResource(R.mipmap.pm25_good);
                                            tv_overall.setText("Good");
                                        }
                                        else if(overall >= 51 && overall <=100){
                                            imageView_overall.setImageResource(R.mipmap.pm25_moderate);
                                            tv_overall.setText("Moderate");
                                        }
                                        else if(overall >= 101 && overall <=200){
                                            imageView_overall.setImageResource(R.mipmap.pm25_unhealthy);
                                            tv_overall.setText("Unhealthy");
                                        }
                                        else if(overall >= 201 && overall <=300){
                                            imageView_overall.setImageResource(R.mipmap.pm25_very_unhealthy);
                                            tv_overall.setText("Very unhealthy");
                                        }
                                        else if(overall > 300){
                                            imageView_overall.setImageResource(R.mipmap.pm25_hazardous);
                                            tv_overall.setText("Hazardous");

                                        }

                                        editor.putInt("psi_west", value_west);
                                        editor.putInt("psi_east", value_east);
                                        editor.putInt("psi_north", value_north);
                                        editor.putInt("psi_south", value_south);
                                        editor.putInt("psi_central", value_central);
                                        editor.putInt("psi_overall", overall);

                                        editor.commit();
                                    }
                                }
                                else{
//                                    editor.putInt("psi_west", 0);
//                                    editor.putInt("psi_east", 0);
//                                    editor.putInt("psi_north", 0);
//                                    editor.putInt("psi_south", 0);
//                                    editor.putInt("psi_central", 0);
//                                    editor.putInt("psi_overall", 0);
//                                    editor.commit();

                                    Log.e("test A","here");

                                    tv_west.setText(settingDetails.getInt("psi_west", 0));
                                    tv_east.setText(settingDetails.getInt("psi_east", 0));
                                    tv_north.setText(settingDetails.getInt("psi_north", 0));
                                    tv_south.setText(settingDetails.getInt("psi_south", 0));
                                    tv_central.setText(settingDetails.getInt("psi_central", 0));
                                    tv_overall.setText(settingDetails.getInt("psi_overall", 0));
                                }


                            } catch (JSONException e) {
                                Log.e(TAG, e.getMessage(), e);

                                Toast.makeText(getContext(),
                                        "Error: " + e.getMessage(),
                                        Toast.LENGTH_LONG).show();
                            }

                        }
                    },
                    new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError e) {
                            Log.e(TAG, e.getMessage(), e);

                            Toast.makeText(getContext(), "Internet Error "+e.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    }) {
                /**
                 * Passing some request headers*
                 */
                @Override
                public Map getHeaders() {
                    HashMap headers = new HashMap();
                    headers.put("AccountKey", "");
                    headers.put("Accept", "application/json");

                    return headers;
                }
            };

            queue.add(jsonObjReq);

//for handling changes
//    pageViewModel.getText().observe(this, new Observer<String>() {
//      @Override
//      public void onChanged(@Nullable String s) {
//        //
//
//
//        textView.setText("K: "+regionName);
//
//      }
//     });

        }
        else
        {
            int value_west = settingDetails.getInt("psi_west", 0);
            int value_east = settingDetails.getInt("psi_east", 0);
            int value_central = settingDetails.getInt("psi_north", 0);
            int value_south = settingDetails.getInt("psi_south", 0);
            int value_north = settingDetails.getInt("psi_central", 0);

            tv_west.setText("West   " + String.valueOf(value_west));
            tv_east.setText("East   " + String.valueOf(value_east));
            tv_north.setText("North   " + String.valueOf(value_north));
            tv_south.setText("South   " + String.valueOf(value_south));
            tv_central.setText("Central   " + String.valueOf(value_central));

            tv_west.setTextColor(checkPSILevel(value_west));
            tv_east.setTextColor(checkPSILevel(value_east));
            tv_central.setTextColor(checkPSILevel(value_central));
            tv_north.setTextColor(checkPSILevel(value_north));
            tv_south.setTextColor(checkPSILevel(value_south));

            int overall = settingDetails.getInt("psi_overall", 0);

            if(overall <= 50){
                imageView_overall.setImageResource(R.mipmap.pm25_good);
                tv_overall.setText("Good");
            }
            else if(overall >= 51 && overall <=100){
                imageView_overall.setImageResource(R.mipmap.pm25_moderate);
                tv_overall.setText("Moderate");
            }
            else if(overall >= 101 && overall <=200){
                imageView_overall.setImageResource(R.mipmap.pm25_unhealthy);
                tv_overall.setText("Unhealthy");
            }
            else if(overall >= 201 && overall <=300){
                imageView_overall.setImageResource(R.mipmap.pm25_very_unhealthy);
                tv_overall.setText("Very unhealthy");
            }
            else if(overall > 300){
                imageView_overall.setImageResource(R.mipmap.pm25_hazardous);
                tv_overall.setText("Hazardous");

            }

            Toast.makeText(this.getContext(),"No Internet Connection",Toast.LENGTH_LONG).show();
        }

        return root;
    }


        private final String TAG_INTERNET = "CheckNetwork";

        public boolean isInternetAvailable(Context context)
        {
            NetworkInfo info = (NetworkInfo) ((ConnectivityManager)
                    context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();

            if (info == null)
            {
                Log.d(TAG_INTERNET,"no internet connection");
                return false;
            }
            else
            {
                if(info.isConnected())
                {
                    Log.d(TAG_INTERNET," internet connection available...");
                    return true;
                }
                else
                {
                    Log.d(TAG_INTERNET," internet connection");
                    return true;
                }
            }
    }

    public int checkPSILevel(int value){
        int indicatorColor = ContextCompat.getColor(getContext(),R.color.colorWhite);

        if(value <= 50){
            indicatorColor = ContextCompat.getColor(getContext(),R.color.colorWhite);
        }
        else if(value >= 51 && value <= 100){
            indicatorColor = ContextCompat.getColor(getContext(),R.color.colorLightBlue);
        }
        else if(value >= 101 && value <= 200){
            indicatorColor = ContextCompat.getColor(getContext(),R.color.colorYellow);
        }
        else if(value >= 201 && value <= 300){
            indicatorColor = ContextCompat.getColor(getContext(),R.color.colorOrange);
        }
        else if(value > 300){
            indicatorColor = ContextCompat.getColor(getContext(),R.color.colorRed);
        }

        return indicatorColor;

    }

}
