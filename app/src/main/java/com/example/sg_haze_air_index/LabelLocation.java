package com.example.sg_haze_air_index;

import java.util.HashMap;
import java.util.Map;

public class LabelLocation {
    private String name;
    private Map<String,String> label_location;

    public LabelLocation(String name, Map <String, String> label_location ) {
        this.name = name;
        this.label_location.put("latitude",label_location.get("latitude"));
        this.label_location.put("longitude",label_location.get("longitude"));
    }

    public String getName() {
        return name;
    }

    public Map<String, String> getLabel_location() {
        return label_location;
    }
}
